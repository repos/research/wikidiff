from pyspark.sql import functions as F
from pyspark.sql import DataFrame
from pyspark.sql.session import SparkSession
import fsspec
from workflow_utils import util
from typing import List

from wikidiff.diff import unified_diff

DESIRED_FILE_MB = 250
REPARTITION_THRESHOLD_THRESHOLD = 100

GB_PER_BATCH=600

def determine_configuration(wiki_db: str, mw_snapshot: str):
    """
    For a given wiki_db and snapshot:
    - compute the number of batches so that each batch process around `gb_per_batch` of data
    - number of output partitions so that output files are around the blocksize configured for hdfs (125mb)
    """
    util.fsspec_use_new_pyarrow_api(True)
    fs = fsspec.filesystem('hdfs')
    files = fs.ls(f'/wmf/data/wmf/mediawiki/wikitext/history/snapshot={mw_snapshot}/wiki_db={wiki_db}',detail=True)
    gb = sum([f['size'] for f in files ]) / (1024**3)
    num_batches = int(gb/GB_PER_BATCH) + 1
    num_partitions_per_batch = int((gb*1e3 / DESIRED_FILE_MB) / num_batches) + 1
    return (num_batches, num_partitions_per_batch)


def diff_wiki(spark: SparkSession, wiki_db: str, mw_snapshot: str, hive_dir: str):
    """
    Compute the diff for a given project and snapshot, and append the output in an
    external hive table. The hive table is partitioned by wiki_db and snapshot, thus
    all the batches executed as part of a call to this method end up stored in the same
    sub directory of `hive_dir`.
    """
    num_batches, num_files_per_batch = determine_configuration(wiki_db, mw_snapshot)
    num_partitions = int(spark.conf.get("spark.sql.shuffle.partitions"))
    # the number of partitions of the returned diff dataframe is the configured number of
    # partitions, so the expected file size is desired_file_mb*(num_files_per_batch/num_partitions)
    # if the desired number of output files is much smaller than the  num_partitions_per_batch is
    expected_file_mb = DESIRED_FILE_MB*(num_files_per_batch/num_partitions)
    force_repartition = True if expected_file_mb < REPARTITION_THRESHOLD_THRESHOLD else False

    print(f'{wiki_db} \n\tnum_batches:{num_batches} \n\tnum_files_per_batch:{num_files_per_batch} \n\tnum_partitions:{num_partitions} \n\texpected_file_mb:{expected_file_mb} \n\tforce_repartition:{force_repartition}')
    # return None

    # TODO, check the where clauses, are they appropriate?
    # TODO: can we afford to store to persist e.g. 11tb on the executors? 150 executors ~> 75gb per executor - seems ok?
    # wikitext_history_df = wikitext_history_df.persist(StorageLevel.DISK_ONLY)
    # For now, we read the full wikitext for each batch and filter down before doing a self-join
    wikitext_history_df = (spark.table('wmf.mediawiki_wikitext_history')
        .where(f"""
            snapshot = '{mw_snapshot}'
            AND wiki_db = '{wiki_db}'
            AND page_namespace = 0
            AND page_id > 0
            AND page_title !=''
            AND page_redirect_title = ''""")
    )
    batches = generate_batches(wikitext_history_df, num_batches)

    # return batches

    partition_cols = ['snapshot', 'wiki_db']
    for i, (diff_df, persisted_df) in enumerate(batches):
        print(f'Executing batch {i}/{len(batches)} for {wiki_db}')
        if force_repartition:
            diff_df = diff_df.repartition(num_files_per_batch)
        diff_df.write.partitionBy(partition_cols).mode("append").parquet(hive_dir)
        # after the diffs for a given batch are computed, we can unpersist
        persisted_df.unpersist()

def generate_batches(wikitext_history_df: DataFrame, num_batches):
    """
    Generate a list of batches that splits up the work of joining wikitext with parent wikitext.
    Returns a list of tuples of Dataframes, the first one is the input wikitext dataframe that is
    cached (so that we can unpersist it after the batch has completed), the second is the dataframe
    the diff column itself.
    """
    batches = []

    for batch in range(num_batches):
        # TODO investigate a good hashing function
        # (wiki_db, page_id)? page_id backfilled?
        # (wiki_db, page_title)? title is unstable?
        # page_hash = F.udf(lambda pt: (hash(pt) % num_batches) == batch, 'boolean')
        page_hash = F.udf(lambda pid: (hash(pid) % num_batches) == batch, 'boolean')

        wikitext_history_batch_df = wikitext_history_df.where(page_hash("page_id"))

        # cache on the filtered level in preparation of the self join
        wikitext_history_batch_df = wikitext_history_batch_df.cache()

        # big self join on wikitext revision_id and revision_parent_id.
        cond = [F.col("c.revision_parent_id") == F.col("p.revision_id"),
                F.col("c.wiki_db") == F.col("p.wiki_db")]
        revision_data_df = (wikitext_history_batch_df.alias("c")
            .join(wikitext_history_batch_df.alias("p"), on=cond, how="left")
            .select("c.*", F.col("p.revision_text").alias("parent_revision_text"))
        )

        revision_diff_df = (revision_data_df
            .withColumn("diff", unified_diff('revision_text', 'parent_revision_text'))
            .drop('parent_revision_text')
        )

        batches.append((revision_diff_df, wikitext_history_batch_df))

    return batches

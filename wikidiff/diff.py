from diff_match_patch import diff_match_patch
from pyspark.sql import functions as F

@F.udf(returnType='string')
def unified_diff(revision_text: str, parent_revision_text: str) -> str:
    """
    PySpark UDF to compute a unified diff between to strings
    """
    if not parent_revision_text:
        return None
    dmp = diff_match_patch()
    dmp.Diff_Timeout = 3
    patches = dmp.patch_make(revision_text, parent_revision_text)
    return dmp.patch_toText(patches)

@F.udf(returnType='string')
def apply_diff(revision_text: str, diff: str) -> str:
    """
    PySpark UDF to apply a diff to a string
    """
    if not diff:
        return None
    dmp = diff_match_patch()
    patch = dmp.patch_fromText(diff)
    parent_revision_text, _ = dmp.patch_apply(patch, revision_text)
    return parent_revision_text


def get_patch(diff: str):
    dmp = diff_match_patch()
    return dmp.patch_fromText(diff)

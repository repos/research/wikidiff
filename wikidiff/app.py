import argparse
from pyspark.sql import SparkSession

#%%
# example config for interactive work.
# note that the entry point methods need to be configured via args, e.g. from irflow
example_conf =  {
    "spark.dynamicAllocation.maxExecutors": 199,
    "spark.executor.cores": 4,
    "spark.sql.shuffle.partitions": 3999,
    "spark.executor.memory": "16g",
    "spark.executor.memoryOverhead": "2g",
    "spark.shuffle.io.retryWait": "60s",
    "spark.network.timeout": "900s",
    "spark.jars.packages": "org.apache.spark:spark-avro_2.12:3.1.2"
}
#%%
def backfill():
    """
    Entry point to create a diff dataset for a mediawiki_wikitext_history snapshot.

    The --projects arg is used to pass a list of wikedia projects, they will be
    processed sequentially using the same spark context. It is preferable to iterate
    over a list of wikis at the call site (e.g. from an airflow dag), which allows
    to configure the spark context based on the size of the wiki and allows paralell execution.
    """

    #TODO test paralell execution on airflow side, does a concurrent append to the same
    # external hive directory work?
    #TODO in airflow, use https://airflow.apache.org/docs/apache-airflow/stable/configurations-ref.html#max-active-tasks-per-dag

    parser = argparse.ArgumentParser()


    parser.add_argument('--hive_table_dir',
                        required=True,
                        help="""The directory of the external hive table. Each job/batch
                        will append to that table, which is partitioned by wiki_db and
                        snapshot""")

    parser.add_argument('--mw_snapshot',
                    required=True,
                    help='the mediawiki snapshot to process')

    parser.add_argument('--projects',
                        required=True,
                        nargs='+',
                        help="""List of projects to process""")

    args = parser.parse_args()

    spark = SparkSession.builder.getOrCreate()

    from wikidiff.batching import diff_wiki

    for wiki_db in args.projects:

        diff_wiki(spark, wiki_db, args.mw_snapshot, args.hive_table_dir)



# Wikidiff

A python package to generate datasets of diffs between Wikipedia article revisions. It uses the [diff_match_patch](https://github.com/google/diff-match-patch) library for generating unified diffs between two article revision wikitexts

Spark pipelines are used to create diff datasets of the full revision history, based on the [wmf.mediawiki_wikitext_history](https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Content/Mediawiki_wikitext_history). The output dataset includes an additional column `diff` that is a patchset (a unified diff), that can be applied to the `revision_text` of the current row to obtain the wikitext of the `parent_revision_id`.

The package also provides pyspark UDFs to work with the diffs. See the notebook [diffing.ipynb](todo) for details on the diffing.

## Approach

It is computationally expensive to generate diffs at scale; to compute a diff we need the revision text and parent revision text. The available datasources contain rows with a single revision text. The join required to get a revision text and its parent revision text is expensive and does not scale well for large wikis. The approach:
- split the job by wiki, which is benetifial since the wiki_db is a partition key of the mediawiki_wikitext_history
- split the per wiki jobs in batches, configurable per wiki depending on its size
- the batches are created by partitioning the revisions using a hashing function that ensures the all revisions of given page are in the same partition
- for each batch, use a self-join on the `revision_id` and `parent_revision_id` and compute the unified diff between the revision texts
- append the output datasets of all batches to a external hive dataset partitioned by snapshot and wiki_db

## Development

### Backfill

The entry point is `wikidiff.app:backfill`. Development is most conveniently done interactively, e.g. see the notebooks folder. To deploy via command line (`CONDA_ENV` can be a local env built via `conda pack` or gitlab url to a env built via CI):

```
pip install .
export CONDA_ENV=./conda.tar.gz
PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./env/bin/python \
spark3-submit \
    --master yarn \
    --driver-memory 4G \
    --num-executors 96 \
    --executor-cores 4 \
    --executor-memory 16G \
    --conf spark.sql.shuffle.partitions=2000 \
    --conf spark.shuffle.io.retryWait=60s \
    --conf spark.network.timeout=1200s \
    --archives $CONDA_ENV#env \
    $(which backfill.py) \
    --hive_table_dir xxx \
    --mw_snapshot 2023-02 \
    --projects frwiki
```